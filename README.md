# Retro Memory Viewer

An interactive memory map for old home computers and other retro devices. Built using Vue.

***STILL EARLY WIP CURRENTLY, REPOSITORY ONLY FOR REFERENCE***

This project's aim is to provide an easy way for retro hardware enthusiasts to have an easy way of viewing and searching through the different memory maps of various older hardware, in order to simplify the process and avoid needing to go through all sorts of documentation.
Hardware, that's not listed on the site as soon as the project's foundation is done, can be added via merge requests to this repository. Currently planned for initial release are Commodore 64 and Gameboy.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
